#!/bin/bash
# Bash scripts to automate code deploy to ASL servers
PULL_BRANCH=develop
LOG_FILE=/var/log/asldeploy.log
PROJECT_DIRECTORY=/home/aslcomm/app


# Create log if it doesn't exist
test -f "$LOG_FILE" || touch "$LOG_FILE"

printf "\n\n" &> $LOG_FILE
echo "====== Pulling fresh code from server ======" &> $LOG_FILE

# Pull the latest changes
(cd "$PROJECT_DIRECTORY" && git pull -u origin "$PULL_BRANCH")

# Stop all node instances
# Yea, this is why we needed the service. So we don't have to be douchebags like this
killall node &> $LOG_FILE

(cd "$PROJECT_DIRECTORY" && npm start)

echo "====== Deployment complete ======"