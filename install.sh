#!/bin/bash
# Bash scripts to install code deploy scripts
NC='\033[0m'
GREEN="\033[0;32m"
YELLOW="\033[1;33m"

SSH_PUBLIC_KEY=/root/.ssh/id_rsa.pub

printf "${GREEN}ASL Deploy Script Installer${NC}"

printf "\n\n"

# Installing deps
echo Installing dependencies...

apt-get update &> /dev/null
apt-get install python3 -y &> /dev/null

# Copy files to relevant directories
echo Copying script files...

mkdir -m 775 -p /opt/asldeploy &> /dev/null
cp ./* /opt/asldeploy &> /dev/null

# Setting permissions on files
echo Updating file permissions...

chmod +x /opt/asldeploy/staging.sh &> /dev/null
chmod +x /opt/asldeploy/deploy.py &> /dev/null

# Generating SSH keys

echo Generating SSH keys...

# Check if root doesn't have a public key added, if it does
test -f "$SSH_PUBLIC_KEY" ||  ssh-keygen -q -t rsa -N '' -f /root/.ssh/id_rsa 2>/dev/null <<< y >/dev/null

## Begin installing the service
echo Installing service...

# If the service was installed already, stop it (prevents port rebinding when updating)
systemctl stop asldeploy &> /dev/null

# Copy the service
cp deploy.service /lib/systemd/system/asldeploy.service &> /dev/null

# Enable and start the service
systemctl daemon-reload &> /dev/null
systemctl enable asldeploy &> /dev/null
systemctl start asldeploy &> /dev/null

printf "\n\n"
printf "${YELLOW}Please add the following public key to BitBucket\n"
printf "\n"
cat /root/.ssh/id_rsa.pub

printf "\n\n${GREEN}Done!\n\n"