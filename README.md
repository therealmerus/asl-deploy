# ASL Deployment Script

So this script acts as a proxy between BitBucket and our server.

In BitBucket we can setup webhooks which basically means that, we can set rules like:

When Welington merges a pull request into develop, visit this url. And on the other end of that URL is this script.

What it then does is, takes down the running application, pull the latest changes from BitBucket and then recompile the application. Automatically. 

This still keeps control in our hands because we are the ones who set on what event this script should be invoked. 

Also, because the script that then gets run is basically a copy paste of most of the commands Jim and Dave would manually 
execute. It just takes that process off our hands; less steps, less mistakes, ideally at leasy


## How to use

1. Download script on server with application
2. Modify staging.sh to reflect install directory of project
3. Run the install script (`sudo bash install.sh`)
4. Configure BitBucket to hit `SERVER_NAME:8084?dhash=[The deployment key below]` on certain events (as needed, the script will handle stopping the app, getting the latest code, restarting it)
5. Merry Christmas

## Upcoming updates

1. So, ideally the application should be started by a service. This way we can track logs (using journalctl) and importantly, the appplication is always started from a central location hence stopping and starting it is fairly easy (instead of it being started by rogue processes)

## Important tid-bits

##### Changing the state of the deployment service

```bash
$>  systemctl start/stop asldeploy
```

##### Checking status of the deployment service 

```bash
$>  systemctl status asldeploy
```

##### Checking logs of the deployment service 

```bash
$>  journalctl -u asldeploy
```

##### Deployment key: 
`7uE0KBOgES4kmCaRkF3yi86S3lSGCZGTpAv8PxGctx07jggq`