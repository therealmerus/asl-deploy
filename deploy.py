#!/usr/bin/python3

from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse
import subprocess


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    DEPLOYMENT_HASH = "7uE0KBOgES4kmCaRkF3yi86S3lSGCZGTpAv8PxGctx07jggq"

    """
    Get the deployment hash for this
    """

    def get_deployment_hash(self):

        try:
            query = urlparse(self.path).query
            query_components = dict(qc.split("=") for qc in query.split("&"))
        except:
            return ""

        return str(query_components["dphash"])

    """
    Handler for when user accesses this without a deploy hash
    """

    def on_permissions_response(self):
        self.send_response(403)
        self.end_headers()
        self.wfile.write(b'{"status": "Failed", "message": "No permission to access resource"}')

    """
    Handler for when all has gone good
    """

    def on_success_response(self):
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b'{"status": "Success", "message": "Deploy started"}')

    """
    Handler for incoming requests
    """

    # noinspection PyPep8Naming
    def do_GET(self):
        deployment_hash = self.get_deployment_hash()

        if not deployment_hash or len(deployment_hash) == 0 or deployment_hash != self.DEPLOYMENT_HASH:
            return self.on_permissions_response()

        subprocess.call('/opt/asldeploy/staging.sh')

        return self.on_success_response()


httpd = HTTPServer(('0.0.0.0', 8084), SimpleHTTPRequestHandler)
httpd.serve_forever()
